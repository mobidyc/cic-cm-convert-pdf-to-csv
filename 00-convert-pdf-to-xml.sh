#! /bin/bash
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent

Usage() {
    cat << EOT
    $0 [ -P <pdf dir> ] [ -X <xml dest> ]

    <pdf dir>: A directory containing all the pdf files.
    <xml dest>: A directory to put the xml output files.

    # Example
    $0 -P $PWD/pdf/ -X xml

    once a pdf has been processed, it will be moved from pdf/file.pdf to pdf/done/file.pdf
    pre-requisite: needs pdftohtml binary
EOT
}

while [ "$#" -ne "0" ];
do
   case $1
      in
      "-P") pdf=$2 ; shift 2 ;;
      "-X") xml=$2 ; shift 2;;
      *) Usage ; exit 1 ;;
   esac
done

src=${pdf:-pdf}
src=$(realpath "$src")
src_done="${src}/done"

dst=${xml:-xml}
dst=$(realpath "$dst")

work_dir=$PWD

[ -d "$src" ] || {
   echo "missing $src directory"
   Usage
}
[ -d "${src_done}" ] || mkdir -p "${src_done}"
[ -d "$dst" ] || mkdir -p "$dst"

cd "$src"
for file in *.pdf
do
   pdftohtml -p -s -i -xml "$file" >/dev/null
   [ "$?" = "0" ] && {
      mv "${file%.pdf}.xml" "${dst}/"
      mv "$file" "${src_done}/"
      echo "${file} done!"
   }
done

# Come back to old directory
cd "$work_dir"

echo "$0 finished"
