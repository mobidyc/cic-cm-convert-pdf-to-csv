# French version
- Converti les extraits de comptes PDF du CIC-CM en CSV avec categorisation.

## Comment utiliser

1. Connectez-vous sur le CIC-CM (cic.fr) et récupérez vos extraits de comptes.
déposez les fichiers PDF dans un dossier ./pdf/

2. lancez le programme "./00-convert-pdf-to-xml.sh"
cela va scanner le dossier ./pdf/, convertir en XML (dans un dossier ./xml/) et déplacer les fichiers terminés dans ./pdf/done/

3. lancez le programme "./01-convert-xml-to-csv.sh" 
cela va scanner le dossier ./xml/ et convertir en CSV (dans un dossier ./csv/ et déplacer les fichiers terminés dans ./xml/done/

vos fichiers sont convertis.

4. (optionnel) si vous souhaiter catégoriser et sous-catégoriser vos lignes de comptes.
configurez le fichier config/default.json
lancez le programme "./02-categorize.py".


- N'hésitez pas à utiliser l'argument --help sur ces programmes pour avoir de l'aide.



# English version
- Convert CIC-CM PDF bank statement to CSV with categorization.

## How to use

1. Connect to CIC-CM (cic.fr) and download you bank statements.
put the PDF files in a folder ./pdf/

2. Run the "./00-convert-pdf-to-xml.sh" program
It will scan the ./pdf/ folder, Convert the files in XML and will move the processed files into ./pdf/done/

3. Run the "./01-convert-xml-to-csv.sh" program
It will scan the ./xml/ folder, Convert the files in CSV and will move the processed files into ./xml/done/

your files are converted.

4. (optional) If you want to categorize your steatement lines.
Edit the ./config/default.json file
run the "./02-categorize.py" program


- Do not hesitate to use the --help argument on these program to get help
