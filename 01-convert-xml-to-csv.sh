#!/bin/bash
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent

Usage() {
   cat << EOT
   $0 [ -X <xml dir> ] [ -C <csv dest> ]

   <xml dir>: A directory containing all the xml files.
   <csv dest>: A directory to put the csv output files.

   # Example
   $0 -X $PWD/xml/ -X csv

   Note:
   An xml file processed is moved to from xml/file.xml to xml/done/file.xml
EOT
}

while [ "$#" -ne "0" ];
do
   case $1
   in
      "-X") xml=$2 ; shift 2;;
      "-C") csv=$2 ; shift 2 ;;
      *) Usage ; exit 1 ;;
   esac
done

src=${xml:-xml}
src=$(realpath "$src")
src_done="${src}/done"

dst=${csv:-csv}
dst=$(realpath "$dst")

work_dir=$PWD

[ -d "$src" ] || {
   echo "missing $src directory"
   Usage
}
[ -d "$src_done" ] || mkdir "$src_done"
[ -d "$dst" ] || mkdir -p "$dst"


###
# Logic explained:
# account name contains ' N° ', ex:
#   C/C CONTRAT PERSONNEL GLOBAL N° 00020134561 en euros (GD)
# account number is in the account name
# end of account data line contains '<b>SOLDE'

function contains_account() {
   # parse a string and detect an account number
   grep -q ' N° [0-9]' <<< "${*}"
}

function is_account_ending() {
   # parse a string and detect the end of account listing
   grep -q '<b>SOLDE' <<< "${*}"
}

function get_account_name() {
   # parse a string and extract the account name
   grep -o '<b>.*' <<< "${*}" |cut -d'>' -f2 |cut -d'<' -f1
}

function get_account_nb() {
   # parse a string and extract the account number
   grep -o ' N° [0-9].*' <<< "${*}" |awk '{print $2}'
}

function extract_text() {
   sed 's@<[/]*b>@@g' <<< "${*}" | \
      sed 's@</text>$@@' | \
      awk -F'>' '{print $NF}'
}

function is_date() {
   awk -F'/' 'BEGIN{err = 0} {
      if(NF != 3) { err = 1 }
      if($1 > 31) { err = 1 }
      if($2 > 12) { err = 1 }
      if($3 < 2000 && $3 > 2100) { err = 1 }
   }
   END{
      exit err
   }' <<< "${*}"
   rval=$?
   return $rval
}

function is_garbage() {
   # return true if garbage is detected
   egrep -q '^$|^Page [0-9]|^Date$|^Date valeur$|^Opération$|^Débit EUROS$|^Crédit EUROS$|^€$|Suite au verso' <<< "${*}" && return 0
   return 1
}

function is_number() {
   val=$(echo "${*}" |sed 's@-\+[0-9],\.@@g')
   [ -n "$val" ]
   return $?
}

function get_account_data() {
   file=$1
   dest=$2

   processing=false

   date=
   dateval=
   label=
   credit=
   debit=
   while read -r linenum line
   do
      if $processing ; then
         is_account_ending "$line" && processing=false && continue
      else
         contains_account "$line" && {
            processing=true
            account_name=$(get_account_name "$line")
            account_nb=$(get_account_nb "$account_name")
            echo
            echo "Processing: $account_nb / $account_name"
         } || continue
      fi

      if $processing ; then
         text=$(extract_text "$line")
         if is_garbage "$text" ; then continue ; fi

         # we have to detect the following 4 or 5 fields:
         # date, datavaleur, label, credit/debit ,(optional: a second title line)

         [ -z "$date" ] && {
            is_date $text && date=$text
            continue
         }
         [ -z "$dateval" ] && {
            is_date $text && dateval=$text
            continue
         }
         [ -z "$label" ] && {
            label=$text
            continue
         }
         [ -z "${debit}${credit}" ] && {
            is_number $text && {
               # get the position to know if it is a debit or a credit
               # and replace 1.200,34 to 1200.34
               position=$(grep -o ' left=".*' <<< "$line" |cut -d'"' -f2)
               [ "$position" -gt "700" ] && credit=$(echo $text |tr -d '.' |tr "," ".") || debit="-$(echo $text |tr -d '.' |tr "," ".")"
            }
            continue
         }

         is_date $text && {
            echo "${date};${dateval};${debit};${credit};$(echo ${label} |sed -e 's@&amp;@\&@g' -e 's@&#34@@g' -e 's@;@@g')" |tr -s " " |tee -a "${dest}/${account_nb}.csv"
            date=$text
         } || {
            echo "${date};${dateval};${debit};${credit};$(echo "${label} ${text}" |sed -e 's@&amp;@\&@g' -e 's@&#34@@g' -e 's@;@@g')" |tr -s " " |tee -a "${dest}/${account_nb}.csv"
            date=
         }

         # reinit variables
         dateval=
         label=
         debit=
         credit=

         continue
      fi
   done < "${file}"
   mv "${file}" "${src_done}/"
}

cd "$src"
for f in *.xml
do
   get_account_data "$f" "$dst"
done
cd "$work_dir"

echo "xml files processed"