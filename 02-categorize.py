#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent

import sys
sys.dont_write_bytecode = True

import json
import csv
from operator import itemgetter
import re
import os

import json
from pprint import pprint as pp
import time
import random
import threading
import traceback
from getopt import getopt, GetoptError

def py2_unicode_to_str(u):
   # unicode is only exist in python2
   #assert isinstance(u, unicode)
   return u.decode('utf-8')


def filter(CSV_INPUT, config="", debug=False):
   account, extension = CSV_INPUT.split('.')
   CSV_OUTPUT = "{}-CAT.{}".format(account, extension)
   try:
      with open(config) as f:
         data = json.load(f)
   except:
      usage()
      sys.exit(1)

   reader = csv.reader(open(CSV_INPUT), delimiter=";")

   # Be sure to use unicode
   CSV_CONTENT = []
   for line in reader:
      line[4] = py2_unicode_to_str(line[4])
      CSV_CONTENT.append(line)

   for category in data.keys():
      for subcategory in data[category]:
         filters = data[category][subcategory]
         if filters:
            for filter in filters:
               if filter:
                  for line in CSV_CONTENT:
                     if re.search(filter, line[4]):
                        if debug:
                           print "[{}] matches [{}] in {}{}".format(line[4].encode('utf-8'), filter, category, subcategory)
                        line.append(category)
                        line.append(subcategory)

   for line in CSV_CONTENT:
      i = len(line)
      if i > 7:
         print "Multi category match for: [{0}]".format(line[4].encode('utf-8'))

   with open(CSV_OUTPUT, 'wb') as csvfile:
      writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
      for line in CSV_CONTENT:
         line[4] = line[4].encode('utf-8')
         writer.writerow(line)

   print "\n{0} written".format(CSV_OUTPUT)

   i = j = 0
   for line in sorted(CSV_CONTENT, key=itemgetter(4)):
      if len(line) > 5:
         i += 1
      else:
         j += 1
   print "{0} lines categorized".format(i)
   print "{0} lines to categorize".format(j)

def usage():
   message = """
Usage: {progname} -c|--csv <csv_file> [-C|--config <config_file>]

   -c, --csv: _________ The csv file to analyze
   -C, --config _______ A json file containing the categories/subcategories to filter.
                        Default to config/default.json.
                        if config/<account_number>.json exists, default to this file.

   example:
   {progname} --csv csv/00075634400.csv --config filters.json

   output will reside in the same directory:
   \t csv/00075634400-CAT.csv

   """.format(progname=sys.argv[0])
   print message

if __name__ == '__main__':
   default_config_file = "config/default.json"
   config_file = default_config_file
   debug = False

   shrt_args = "hc:C:d"
   long_args = ["help", "csv=", "config=", "debug"]
   try:
      opts, _ = getopt(sys.argv[1:], shrt_args, long_args)
   except GetoptError:
      print "Argument error"
      usage()
      sys.exit(1)

   for opt, arg in opts:
      if opt in ("-h", "--help"):
         usage()
         sys.exit(0)

      elif opt in ("-c", "--csv"):
         if not os.path.isfile(arg):
            print "{} is not a file".format(arg)
            usage()
            sys.exit(1)
         csv_file = arg
      elif opt in ("-C", "--config"):
         if not os.path.isfile(arg):
            print "{} is not a file".format(arg)
            usage()
            sys.exit(1)
         config_file = arg
      elif opt in ("-d", "--debug"):
         debug = True

   if not "csv_file" in locals():
      print "Missing csv file to parse"
      usage()
      sys.exit(1)

   # do we use the default config file?
   if config_file is default_config_file:
      # check if we have a better config file to use
      account, _ = csv_file.split('.')
      better_config = "config/{}.json".format(os.path.basename(account))
      if os.path.isfile(better_config):
         config_file = better_config
   print "Using {} as a config filter".format(config_file)

   filter(csv_file, config = config_file, debug = debug)